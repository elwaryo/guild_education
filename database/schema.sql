SET role guild_sample_usr;

DROP SCHEMA IF EXISTS movie CASCADE;
CREATE SCHEMA movie;
SET search_path TO movie,public;


CREATE TABLE movie.genre (
    id      INTEGER PRIMARY KEY,
    name    VARCHAR NOT NULL
);

CREATE TABLE movie.movie (
    id      INTEGER PRIMARY KEY,
    title   VARCHAR NULL,
    budget  NUMERIC(15) NULL,
    revenue NUMERIC(15) NULL,
    year_released CHAR(4) NULL,
    popularity NUMERIC(12,6) NULL
);

CREATE TABLE movie.production_company (
    id      INTEGER PRIMARY KEY,
    name    VARCHAR NOT NULL
);

CREATE TABLE movie.movie_genre (
    movie_id    INTEGER NOT NULL,-- REFERENCES movie.movie(id),
    genre_id    INTEGER NOT NULL,-- REFERENCES movie.genre(id),
    PRIMARY KEY (genre_id, movie_id)
);


CREATE TABLE movie.movie_production_company(
    movie_id                INTEGER NOT NULL , --REFERENCES movie.movie(id),
    production_company_id   INTEGER NOT NULL , --REFERENCES movie.production_company(id),
    PRIMARY KEY (production_company_id, movie_id)
);
