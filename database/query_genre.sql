SET search_path TO movie,public;

DROP TABLE IF EXISTS _q3;
CREATE TEMPORARY TABLE _q3 AS
-- Genre Details:
--      most popular genre by year
--
WITH genre_popularity_by_year AS
(
    SELECT          genre.id,
                    genre.name AS genre_name,
                    movie.year_released,
                    ROUND(AVG(COALESCE(movie.popularity, 0)), 3) AS average_popularity
    FROM            genre
    LEFT OUTER JOIN movie_genre
    ON              movie_genre.genre_id = genre.id
    LEFT OUTER JOIN movie
    ON              movie.id = movie_genre.movie_id
    GROUP BY        genre.id,
                    genre.name,
                    movie.year_released
    ORDER BY        movie.year_released DESC NULLS LAST,
                    average_popularity DESC
),
genre_popularity_by_year_ranked AS (
    SELECT  id, genre_name, year_released, average_popularity,
            rank() OVER (
                -- Randomness added to the sort as a tie breaker
                -- when two genres have the same popularity
                PARTITION BY year_released ORDER BY average_popularity DESC, random()
            )
    FROM    genre_popularity_by_year
)
SELECT      genre_name, year_released, average_popularity
FROM        genre_popularity_by_year_ranked
WHERE       rank = 1
ORDER BY    year_released DESC NULLS LAST
;

\copy _q3 TO '../answers/q3.csv' WITH DELIMITER E'\t' CSV HEADER



DROP TABLE IF EXISTS _q4;
CREATE TEMPORARY TABLE _q4 AS
-- Genre Details:
--      most popular genre by year
--      budget by genre by year
--      revenue by genre by year
--      profit by genre by year
--
SELECT          genre.id,
                genre.name AS genre_name,
                movie.year_released,
                ROUND(SUM(COALESCE(movie.budget, 0))) AS total_budget,
                ROUND(SUM(COALESCE(movie.revenue, 0))) AS total_revenue,
                ROUND(SUM(COALESCE(movie.revenue, 0)) - SUM(COALESCE(movie.budget, 0))) AS total_profit
FROM            genre
LEFT OUTER JOIN movie_genre
ON              movie_genre.genre_id = genre.id
LEFT OUTER JOIN movie
ON              movie.id = movie_genre.movie_id
GROUP BY        genre.id,
                genre.name,
                movie.year_released
ORDER BY        genre.name
;


\copy _q4 TO '../answers/q4.csv' WITH DELIMITER E'\t' CSV HEADER
