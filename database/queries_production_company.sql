SET search_path TO movie,public;

DROP TABLE IF EXISTS _q1;
CREATE TEMPORARY TABLE _q1 AS
-- Production Company Details:
--      budget per year
--      revenue per year
--      profit per year
--      average popularity of produced movies per year
--
SELECT          production_company.id,
                production_company.name AS production_company_name,
                movie.year_released,
                ROUND(SUM(COALESCE(movie.budget, 0))) AS total_budget,
                ROUND(SUM(COALESCE(movie.revenue, 0))) AS total_revenue,
                ROUND(SUM(COALESCE(movie.revenue, 0)) - SUM(COALESCE(movie.budget, 0))) AS total_profit,
                ROUND(AVG(COALESCE(movie.popularity, 0)), 3) AS average_popularity
FROM            production_company
LEFT OUTER JOIN movie_production_company
ON              movie_production_company.production_company_id = production_company.id
LEFT OUTER JOIN movie
ON              movie.id = movie_production_company.movie_id
GROUP BY        production_company.id,
                production_company.name,
                movie.year_released
ORDER BY        production_company.name,
                movie.year_released DESC
;

\copy _q1 TO '../answers/q1.csv' WITH DELIMITER E'\t' CSV HEADER


DROP TABLE IF EXISTS _q2;
CREATE TEMPORARY TABLE _q2 AS
-- Production Company Details:
--      releases by genre per year
--
SELECT          production_company.id,
                production_company.name AS production_company_name,
                movie.year_released,
                genre.name AS genre,
                COUNT(*) AS releases_count
FROM            production_company
LEFT OUTER JOIN movie_production_company
ON              movie_production_company.production_company_id = production_company.id
LEFT OUTER JOIN movie
ON              movie.id = movie_production_company.movie_id
LEFT OUTER JOIN movie_genre
ON              movie_genre.movie_id = movie.id
LEFT OUTER JOIN genre
ON              genre.id = movie_genre.movie_id
WHERE           genre.name IS NOT NULL
GROUP BY        production_company.id,
                production_company.name,
                movie.year_released,
                genre.name
ORDER BY        production_company.name,
                genre.name DESC,
                movie.year_released DESC
;

\copy _q2 TO '../answers/q2.csv' WITH DELIMITER E'\t' CSV HEADER
