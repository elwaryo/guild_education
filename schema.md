

# File descriptions

## credits.csv:

    cast,crew,id
    
### cast
JSON array of cast members. Sample cast member JSON:
```json
{
    "cast_id":1,
    "character":"Alan Parrish",
    "credit_id":"52fe44bfc3a36847f80a7c73",
    "gender":2,
    "id":2157,
    "name":"Robin Williams",
    "order":0,
    "profile_path":"/sojtJyIV3lkUeThD7A2oHNm8183.jpg"
}
```

### Crew
JSON array of crew members. Sample crew member JSON:
```json
{
    "credit_id":"52fe44bfc3a36847f80a7cd1",
    "department":"Production",
    "gender":2,
    "id":511,
    "job":"Executive Producer",
    "name":"Larry J. Franco",
    "profile_path":"None"
   }
```
### id
Integer value not sure what it's purpose is
probably a unique ID for the movie. Sample value
```
862
```



## keywords.csv:

    id, keywords

### id
Integer value not sure what it's purpose is
probably a unique ID for the movie.

Sample value
```
862
```

### keywords
JSON array of keywords. Sample keyword JSON:
```json
{
    "id":931,
    "name":"jealousy"
}
``` 

## links_small.csv & links_small.csv

    movieId,imdbId,tmdbId
    
This seems to be a file that provides cross reference data
between movieId, imdbId, and tmdbId

Sample row:
```
1,0114709,862
``` 

## movies_metadata.csv

    adult, belongs_to_collection, budget, genres, homepage,
    id, imdb_id, original_language, original_title, overview,
    popularity, poster_path, production_companies,
    production_countries, release_date, revenue, runtime,
    spoken_languages, status, tagline, title, video,
    vote_average, vote_count

###adult
True or false flag indicating "adult content"??

###belongs_to_collection
JSON object containing the collection the movie belongs to

```json
{
  'id': 10194,
  'name': 'Toy Story Collection',
  'poster_path': '/7G9915LfUQ2lVfwMEEhDsn3kT4B.jpg',
  'backdrop_path': '/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg'
}
```

###budget
Dollar amount of movie budget. e.g.
```
30000000
```

###genres
JSON array containing genres data. Sample JSON:
```json
[
  {
    'id': 16,
    'name': 'Animation'
  },
  {
    'id': 35,
    'name': 'Comedy'
  },
  {
    'id': 10751,
    'name': 'Family'
  }
]
```

###homepage
URL to the movie's homepage. Sample data:
- http://toystory.disney.com/toy-story

###id
ID of movie. Sample:
```
862
```

###imdb_id
IMDB ID of movie
```
tt0114709
```

###original_language
Original language of movie, sample:
```
en
```

###original_title
Original title of movie, sample:
```
Toy Story
```

###overview
Text describing movie, sample:
```text
Led by Woody, Andy's toys live happily in his room until Andy's birthday brings Buzz Lightyear onto the scene. Afraid of losing his place in Andy's heart, Woody plots against Buzz. But when circumstances separate Buzz and Woody from their owner, the duo eventually learns to put aside their differences.
```

###popularity
Popularity index of movie, sample:
```
21.946943
```

###poster_path
Path to movie poster image
```
/rhIRbceoE9lR4veEXuwCC2wARtG.jpg
```

###production_companies
JSON Array containing the production companies, sample JSON:
```json
[
  {
    'name': 'Pixar Animation Studios',
    'id': 3
  }
]
```

###production_countries
JSON Array containing the production countries, sample JSON:
```json
[
  {
    'iso_3166_1': 'US',
    'name': 'United States of America'
  }
]
```

###release_date
Date movie was release, sample value (ISO format): 
```
1995-10-30
```

###revenue
Dollar amount of movie revenue. Sample value:
```
373554033
```

###runtime
Movie duration. Sample value
```text
81
```

###spoken_languages
JSON array containing the spoken languages the movie is available in
```json
[
  {
    'iso_639_1': 'en',
    'name': 'English'
  }
]
```


###status
Status of the movie. Sample value:
```text
Released
```

###tagline
The text of tagline of the movie. Sample value:
```text
Roll the dice and unleash the excitement!
```

###title
The title of the movie. Sample value:
```text
Toy Story
```

###video
Boolean indicating of the movie is available in home video. Sample value:
```text
False
```

###vote_average
Average movie vote. Sample value:
```text
7.70
```

###vote_count
Number of votes. Sample value:
```text
5415
```


## ratings.csv & ratings_small.csv

    userId,movieId,rating,timestamp

### userId
The ID of the user that rated the movie. Sample value:
```text
1
```

### movieId
The ID of the movie. Sample value:
```text
31
```

### rating
The rating of the movie. Sample value:
```text
2.5
```

### timestamp
Unknown. Sample value:
