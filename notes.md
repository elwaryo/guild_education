### Running spark
The `postgresql-42.2.14.jar` must be in the `/usr/local/spark/jars/postgresql-42.2.14.jar --jars /usr/local/spark/jars/` folder
```
/usr/local/spark/bin/spark-shell --driver-class-path /usr/local/spark/jars/postgresql-42.2.14.jar --jars /usr/local/spark/jars/postgresql-42.2.14.jar
```


### Setting up the database
Run the following SQL to create a user and database
```sql
CREATE ROLE guild_sample_usr LOGIN PASSWORD 's00per5ecret';
CREATE DATABASE guild_sample WITH OWNER guild_sample_usr;
```


### Check tables are populated

SELECT 'genre'AS table_name, COUNT(*) AS count FROM genre
UNION ALL
SELECT 'movie'AS table_name, COUNT(*) AS count FROM movie
UNION ALL
SELECT 'movie_genre'AS table_name, COUNT(*) AS count FROM movie_genre
UNION ALL
SELECT 'movie_production_company'AS table_name, COUNT(*) AS count FROM movie_production_company
UNION ALL
SELECT 'production_company'AS table_name, COUNT(*) AS count FROM production_company 
;
