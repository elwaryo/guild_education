#!/usr/bin/env python

import argparse
import ast
import csv
import json

json_fields = ['belongs_to_collection', 'genres', 'production_companies', 'production_countries', 'spoken_languages']


def get_contains_json_dictionary(row_dictionary):
    return {key: key in json_fields for key in row_dictionary.keys()}


def make_proper_json(value):
    try:
        return ast.literal_eval(value)
    except:
        return None


def is_valid(value):
    return value is not None and len(value.strip()) > 0


def main():
    parser = argparse.ArgumentParser(
        description='converts a CSV file to a JSON file')
    parser.add_argument('--input-file-pathname', help='path to CSV data file to ingest', required=True)
    parser.add_argument('--output-file-pathname', help='path to write the converted JSON file to', required=True)
    parser.add_argument('--max-rows', help='maximum number of rows to process from the input file', type=int)
    args = parser.parse_args()

    max_rows = args.max_rows if args.max_rows is not None else -1

    with open(args.input_file_pathname) as input_csv, \
            open(args.output_file_pathname, 'w') as output_json:
        output_json.write('[')
        reader = csv.DictReader(input_csv, delimiter=',')
        first = True
        row_number = 0
        contains_json = {}
        for row_dictionary in reader:
            if first:
                contains_json = get_contains_json_dictionary(row_dictionary)
            else:
                output_json.write(',\n')

            fixed_dictionary = {
                key: make_proper_json(row_dictionary[key])
                if is_valid(row_dictionary[key]) and contains_json[key]
                else row_dictionary[key]
                for key in row_dictionary.keys()
            }

            json_string = json.dumps(fixed_dictionary)

            output_json.write(json_string)
            first = False

            row_number += 1
            if 0 < max_rows < row_number:
                break

        output_json.write(']')


if __name__ == "__main__":
    # execute only if run as a script
    main()
