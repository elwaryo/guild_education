
// Grab the schema from a small file
val allMovieDataPartialDf = spark.read.option("multiLine", true).json("../data/movies_metadata_partial.json")
val schema = allMovieDataPartialDf.schema
// Load the full data set using the schema obtained above
val allMovieDataDf = spark.read.schema(schema).json("../data/movies_metadata.json")
// Remove row with null id field (bad data)
// Select the columns needed to perform the analysis needed
val movieDataDf = allMovieDataDf.filter($"id".isNotNull).
    select($"id".as("movie_id"), $"title", $"original_title", $"budget", $"revenue", $"genres", $"production_companies", $"release_date", $"popularity").
    selectExpr("cast(movie_id as int) movie_id", "coalesce(title, original_title, '<unknown_title>') title", "cast(budget as double) budget", "cast(revenue as double) revenue", "genres", "production_companies", "release_date", "cast(popularity as double) popularity")

// Create a data set containing a row for each production company by its ID
val productionCompanyDf = movieDataDf.select(explode($"production_companies").as("production_company")).select($"production_company.*").distinct

// create a data set that links a movie to its production company(s)
val movieProductionCompanyDf = movieDataDf.select($"movie_id", explode($"production_companies.id").as("production_company_id")).select($"movie_id", $"production_company_id")

// create a data set containing a row for each genre
val genreDf = movieDataDf.select(explode($"genres").as("genres")).select($"genres.*").distinct

// create a data set that links a movie to its genre(s)
val movieGenreDf = movieDataDf.select($"movie_id", explode($"genres.id").as("genre_id")).select($"movie_id", $"genre_id")

// create a data set containing the information needed for the analysis for each movie
val movieInformationDf = movieDataDf.select($"movie_id", $"title", $"budget", $"revenue", date_format(to_date($"release_date","yyyy-MM-dd"), "Y").as("year_released"), $"popularity").distinct

// Remove duplicate movies
val uniqueMovieDf = movieInformationDf.groupBy("movie_id").count.where($"count" === 1)
val movieInfoDf = uniqueMovieDf.alias("unique").join(movieInformationDf.alias("all"), $"all.movie_id" === $"unique.movie_id", "inner").
    select($"unique.movie_id".as("movie_id"), $"title", $"budget", $"revenue", $"year_released", $"popularity")


// Export the data to files
import org.apache.spark.sql.SaveMode

genreDf.repartition(1).write.mode(SaveMode.Overwrite).format("csv").option("header", "true").save("../export/genre")

productionCompanyDf.repartition(1).write.mode(SaveMode.Overwrite).format("csv").option("header", "true").save("../export/production_company")

movieInfoDf.select($"movie_id".as("id"), $"title", $"budget", $"revenue", $"year_released", $"popularity").
    filter($"id".isNotNull).distinct.
    repartition(1).write.mode(SaveMode.Overwrite).format("csv").option("header", "true").save("../export/movie")

movieProductionCompanyDf.
    filter($"movie_id".isNotNull).
    distinct.
    repartition(1).write.mode(SaveMode.Overwrite).format("csv").option("header", "true").save("../export/movie_production_company")

movieGenreDf.
    filter($"movie_id".isNotNull).
    distinct.
    repartition(1).write.mode(SaveMode.Overwrite).format("csv").option("header", "true").save("../export/movie_genre")



// IF YOU WISH TO STORE THE DATA IN TABLES IN A POSTGRES
// DB UNCOMMENT THE LINES BELOW AND READ THE README.pdf


// // Insert data into DB
// import org.apache.spark.sql.SaveMode
// import java.sql.DriverManager
//
// val url="jdbc:postgresql://localhost/guild_sample"
// val username = "guild_sample_usr"
// val password="s00per5ecret"
//
// def truncate(tableName: String) = {
//     val connection = DriverManager.getConnection(url, username, password)
//     connection.setAutoCommit(true)
//     val statement = connection.createStatement()
//     statement.execute(s"TRUNCATE TABLE $tableName CASCADE")
// }
//
//
// truncate("movie.movie_production_company")
// truncate("movie.movie_genre")
// truncate("movie.movie")
// truncate("movie.genre")
// truncate("movie.production_company")
//
// genreDf.
//     repartition(1).
//     write.
//     mode(SaveMode.Append).
//     format("jdbc").
//     option("url", url).
//     option("user", username).
//     option("password", password).
//     option("truncate", "true").
//     option("dbtable", "movie.genre").
//     save
//
//
// productionCompanyDf.
//     repartition(1).
//     write.
//     mode(SaveMode.Append).
//     format("jdbc").
//     option("url", url).
//     option("user", username).
//     option("password", password).
//     option("truncate", "true").
//     option("dbtable", "movie.production_company").
//     save
//
//
// movieInfoDf.
//     select($"movie_id".as("id"), $"title", $"budget", $"revenue", $"year_released", $"popularity").
//     filter($"id".isNotNull).
//     distinct.
//     repartition(1).
//     write.
//     mode(SaveMode.Append).
//     format("jdbc").
//     option("url", url).
//     option("user", username).
//     option("password", password).
//     option("truncate", "true").
//     option("dbtable", "movie.movie").
//     save
//
//
// movieProductionCompanyDf.
//     filter($"movie_id".isNotNull).
//     distinct.
//     repartition(1).
//     write.
//     mode(SaveMode.Append).
//     format("jdbc").
//     option("url", url).
//     option("user", username).
//     option("password", password).
//     option("truncate", "true").
//     option("dbtable", "movie.movie_production_company").
//     save
//
//
// movieGenreDf.
//     filter($"movie_id".isNotNull).
//     distinct.
//     repartition(1).
//     write.
//     mode(SaveMode.Append).
//     format("jdbc").
//     option("url", url).
//     option("user", username).
//     option("password", password).
//     option("truncate", "true").
//     option("dbtable", "movie.movie_genre").
//     save
//

